# eslint-config-house-foundation

## Usage

`npm i @house-agency/eslint-config-house-foundation -D`

Then add a `.eslintrc` file to your project that contains:

```
{
  "extends": "@house-agency/eslint-config-house-foundation"
}
```